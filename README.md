# linux-wiiu.org

This is the source code for the linux-wiiu.org website. It's a static site processed via Jekyll and hosted on a private server.

## Layout
Pages are `.markdown` files scattered throughout the repo. See [index.markdown](index.markdown). Jekyll processes these into HTML using the files in `_layouts` and `_includes`. Images go in the `assets` folder.

## Contributing
PRs are very welcome! Writing documentation is hard and non-developers often have the best handle on what's actually useful to read. You can edit the markdown files in any text editor, and if you'd like to test the website, the magic command is:

```sh
bundler exec jekyll serve --livereload
```

You might need to install Ruby and Bundler. On my Fedora machine it's `rubygem-bundler`.
