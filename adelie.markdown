---
layout: default
title: Adélie - linux-wiiu
---
# Adélie Linux
[Adélie Linux](https://www.adelielinux.org/) is a newer, simplicity-oriented distro that is quite fast. It's the only distro linux-wiiu is aware of, other than [Gentoo](/gentoo), with upstream support for PowerPC. Since it's a newer distro though, some software simply isn't available yet.

## Downloads
You can choose between Adélie's offical tarball or customised third-party images.
- **Adélie 1.0-beta5** (2024-04-26) - [Tarball](https://distfiles.adelielinux.org/adelie/1.0-beta5/iso/adelie-rootfs-full-ppc-1.0-beta5-20240426.txz) - [Other Downloads](https://www.adelielinux.org/download/) (select PPC -> Full Rootfs for tarball)
- **puffin9's image** (2023-05-07) - [More Info](https://archive.org/details/adelie-linux-for-wii-u) - [Torrent](/assets/torrents/adelie-linux-for-wii-u.torrent) - [Flashable disk image](https://archive.org/download/adelie-linux-for-wii-u/Adelie-Linux-CLI-for-Wii-U-660MB-USB-Disk-Image.img.xz)

The dates are not particularly relevant here, since any Adélie can update itself from the internet. Once you have downloaded your image, proceed with the [Install](/install) guide.

## Basic Setup
If you're using puffin9's image, a lot of the setup work is already complete, you can skip to (section). At time of writing, a fresh install from tarball required using tty2 (Ctrl-Alt-F2) to get a login screen and complete installation, but this may be fixed down the road. For official Adélie media, the default account is **root** and the password is **live**.

### User Account
The first thing you might want is your own user account, rather than using root all the time.
1. First, create the account with `useradd -s /bin/zsh -m -G input,video,wheel steve` (you can replace steve with any username). The input and video groups will be useful for graphical programs, and wheel will allow us to use sudo later when we install it.
2. Set the password with `passwd steve`.
3. Change root's password (to something other than 'live') with `passwd root`.

Your new account will be ready to use and can be logged into on another tty (Ctrl-Alt-F2). You can get back to your root shell with Ctrl-Alt-F1, where we'll continue.

### Hostname
A nice bit of customisation is the hostname, by default **adelie-live**. Set it to anything with `echo anything > /etc/hostname`. `reboot` to see the change. I like to name mine after eeveelutions :)

You must also update the hosts file with `nano /etc/hosts` and add the hostname after `localhost`. For example, if you chose `wiiu` as your hostname:
```
127.0.0.1       localhost wiiu
::1             localhost wiiu
```

This prevents lookup delays (~2s freezes) in programs like sudo and X11.

### Getting online
At the time of writing, Wi-Fi is **not** supported under linux-wiiu. You'll need to use a USB ethernet dongle, tether an Android phone over USB, or a USB Wi-Fi adapter. (Bluetooth and iPhone tethering are also possible, but the required packages aren't installed in the default Adélie tarball.)

Verify the existence of your network device with `ip l`. This is a Wii LAN adapter:
```
3: eth0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:19:fd:00:00:00 brd ff:ff:ff:ff:ff:ff
```

This is an Android device with USB tethering enabled:
```
4: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 01:23:45:00:00:00 brd ff:ff:ff:ff:ff:ff
```

USB Wi-Fi isn't shown here since it's a bit complex. See [ArchWiki's Network configuration/Wireless page](https://wiki.archlinux.org/title/Network_configuration/Wireless).

Note the prescence of `state DOWN` in the first line of each - they are not connected automatically. To bring them up, we can run `dhcpcd -w eth0` (or `dhcpcd -w usb0` in the Android case). This will run until it receives an IP, which can take a few seconds, and then return you to a shell. You can try to ping some website (e.g. `ping linux-wiiu.org`) to see if you're online.

### Timezone and date
If you run `date`, it's likely the time will be wrong, which will prevent downloading system updates and other files. First we'll change the timezone away from UTC - `rm /etc/localtime`, then `ln -s /usr/share/zoneinfo/Australia/Sydney /etc/localtime`. You can press tab after `zoneinfo/` to see a list of options. Now running `date` will show the correct timezone.

Then we can change the date itself with `date -s "2024-07-12 19:57"` (yyyy-mm-dd hh:mm, in 24hr time).

### Updates
You can fetch updates with `apk update`, then install them with `apk upgrade`. Rebooting after an update is strongly recommended.

### Sudo
`apk add sudo`. Then, we need to say *who* can use sudo! Run `EDITOR=nano visudo` (or use another editor of your choice), find the line which says "Uncomment to allow members of group wheel to execute any command", and uncomment (delete the #) from the *next* line down. If using nano, save and exit with Ctrl-X, then Y, then Enter.

The user account you made before now can use sudo to run setup commands, so now's a good time to switch to it :)

### Xorg
Install a basic X environment: `apk add xorg-server xf86-input-libinput xf86-video-fbdev xinit xterm openbox ttf-dejavu ttf-liberation`

This will give you the `startx` command, but without any application to run, it just quits immediately. Let's run `xterm`, a terminal, and `openbox`, which will give you "windows".

Edit .xinitrc with `nano ~/.xinitrc`, then type:
```
xterm &
exec openbox-session
```

Save and exit (remember, Ctrl-X for nano), then run `startx`. You should get a white terminal with a titlebar, mouse and keyboard support! This is a basic Xorg environment and is suitable for lots of graphical applications.

If you want to learn about customising openbox further, check out [ArchWiki's openbox page](https://wiki.archlinux.org/title/Openbox).

To quit Xorg, right-click on the blank part of the screen, and select "Log out".

## Optional extras
You're set up for basic use, but how about some fun?

### Desktop environments
Openbox is fun and minimal, but you might want a more complete desktop experience. You have plenty to choose from. The core idea is to install a desktop with apk, then replace `openbox-session` with the new environment in your `.xinitrc` file. Refer to the Xorg section above for how to edit that file.

- IceWM: `sudo apk add icewm`; `exec icewm-session` - very aesthetic, no apps built-in, runs well
- LXQt: `sudo apk add lxqt-desktop`; `exec startlxqt` - modern straightforward desktop, has apps, runs okay
- MATE: `sudo apk add mate-complete`; `exec mate-session` - modern desktop, familiar to Linux Mint users, has apps. Runs not great
- XFCE: `sudo apk add xfce-desktop`; `exec startxfce4` - crash looped for me
- KDE: `sudo apk add kde`; `exec startplasma-x11` - full fat modern OS, loads of applications. Runs dog slow. Might be worth installing anyway to use the applications under LXQt

### OpenTTD
OpenTTD is a game about buses, trains, ships, and planes - but mostly trains. You can install it with `sudo apk add openttd`. Use some special flags to help it run smoothly: `openttd -b 8bpp-optimized -s null -m null -r 800x600`. Running from a tty (**not** Xorg) dramatically improves performance and you can try resolutions up to 1280x720.
Note that Adélie ships a fairly old version of the game that still supports SDL1 - the ability to run under a tty may be lost if they ever update to an SDL2 version of the game.

### Java (Minecraft server)
`sudo apk add openjdk8-jre`. This is Java 8, so good enough for Minecraft servers pre-1.17. It'll run like crap, but if you must:
```
curl -o server1.5.jar https://launcher.mojang.com/v1/objects/f9ae3f651319151ce99a0bfad6b34fa16eb6775f/server.jar
java -jar server1.5.jar
```

You can find other server jars somewhere like [MCVersions](https://mcversions.net/).

### Web browser (Lynx)
Lynx is a non-graphical web browser that runs fast and can help with downloading files. `sudo apk add lynx` then `lynx duckduckgo.com`.

### Web browser (NetSurf)
NetSurf is a HTML4 web browser that runs under X and renders apps graphically. `sudo apk add netsurf` then `netsurf-gtk3`. It's good enough for the Dolphin Blog so that's good enough for anybody.

### Web browser (Firefox..?)
At time of writing (2024-07-12), `firefox-esr` isn't in the latest Adélie repos, even though it used to be. Still, try `sudo apk add firefox-esr` in case it's come back!
I also tried a half-dozen "alternative" builds of Firefox (ArcticFox, Faded Orb, Interweb, Sealion, SeaMonkey, SpiderWeb...) and all of them were missing glibc symbols.

Update 2024-10-06: The maintainer of another distro, ArchPOWER, told me that Adélie switched their Firefox build to ppc64 only. This leaves NetSurf as the best option in the ppc 32-bit repos.
