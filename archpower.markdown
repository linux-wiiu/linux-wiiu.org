---
layout: default
title: ArchPOWER - linux-wiiu
---
# ArchPOWER
[ArchPOWER](https://archlinuxpower.org/) is a port of Arch Linux to PowerPC (and others) with the base packages and a few exciting extras (like ArcticFox!). Uniquely, the developer provides images for linux-wiiu specifically, configured with a modular kernel and plenty of extra drivers.

## Downloads
- **ArchPOWER official** (2024-10-06) - [Tarball](https://archlinuxpower.org/wiiu-images/) (unpack this to find actual rootfs tarball + install guide)

The dates are not particularly relevant here, since any ArchPOWER can update itself from the internet. Once you have the image, unpack it and view the README contained within. (Follow that instead of our install guide - this lets you take full advantage of the modular kernel)
