---
layout: default
title: Debian - linux-wiiu
---
# Debian
[Debian](https://www.debian.org/) is a popular and mature distribution, with a wide variety of software. However, it no longer directly supports the PowerPC processor used in the Wii U, so some packages are missing or don't work.

## Downloads
You can choose between **older** images provided by the linux-wiiu project or **newer** images provided by a third party.

> The linux-wiiu project has a large server seeding these images, so please use torrent if you can.
{:.yellow}

- **linux-wiiu CLI** (2024-10-23) - [Torrent](/assets/torrents/debian-wiiu-2024-10-23.tar.xz.torrent) (tarball only, torrent only until archive.org is fully online again) (snapshot 2024-10-23)
- **linux-wiiu's legacy CLI** (2020-03-07) - [Torrent](/assets/torrents/debian-wiiu-2020-03-07.torrent) - [Flashable disk image](https://archive.org/download/debian-wiiu-2020-03-07/debian-wiiu-2020-03-07.img) - [Tarball](https://archive.org/download/debian-wiiu-2020-03-07/debian-wiiu-2020-03-07.tar.xz)
- **puffin9's CLI** (2024-01-29) - [More info](https://archive.org/details/debian-sid-on-wii-u) - [Torrent](/assets/torrents/debian-sid-on-wii-u.torrent) - [Flashable disk image](https://archive.org/download/debian-sid-on-wii-u/Debian-Sid-13-WiiU-Aug5th2024-3.2GB.img.xz)

Consider the age of the image you download - Debian ports regularly breaks its repo (as of mid-2024 at least), so you may find yourself unable to update very far. Once you have downloaded your image, proceed with the [Install](/install) guide.

## Updating
Debian's powerpc support is unofficial and sometimes broken to the point of being unable to update the system or install packages. To combat this, some newer images use [Debian Snapshots](https://snapshot.debian.org/) to fix the distro in time rather than follow the latest updates. This will allow you to install ~anything without issues, but it'll be the older version from when the snapshot was taken.

"You have requested an impossible situation" is one error that appears when the repositories are broken.

If you want to try the latest packages, we need to edit the apt sources. Use `sudo nano /etc/apt/sources.list`. For a snapshots-enabled image, it'll look something like this:
```list
deb https://snapshot.debian.org/archive/debian-ports/20241023T074002Z unstable main
deb https://snapshot.debian.org/archive/debian-ports/20241023T074002Z unreleased main
```

Update the URLs to the following:
```list
deb https://deb.debian.org/debian-ports unstable main
deb https://deb.debian.org/debian-ports unreleased main
```

If using `nano` as suggested, press Ctrl-X to exit, then Y and Enter to save. You can now do `apt update` and `apt upgrade` as usual to get the latest packages.

## Signature errors
If you get signature errors while using apt, and the image you installed from is more than 1 year old, try the following command:
```
curl https://www.ports.debian.org/archive_`date +%Y`.key | apt-key add -
```

Debian Ports updates their keys annually so older images might not contain new enough keys.
