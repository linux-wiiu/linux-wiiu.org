---
layout: default
title: Gentoo - linux-wiiu
---
# Gentoo
[Gentoo](https://www.gentoo.org/) is a **source-based** Linux distribution, so software is compiled as it installed (often at the cost of many hours!). Gentoo is the only distro at current (2025-02) that can have the SMP workaround required for multi-core on Wii U baked in, though ArchPOWER is testing it and Adélie has expressed interest.

## Downloads
Note that for Gentoo you are probably better off getting a cross-compilation setup (detailed below) working, rather than trying to build on your Wii U - the CPU is simply impractically slow. Nevertheless, a third-party image is available:

> The linux-wiiu project has a large server seeding these images, so please use torrent if you can.
{:.yellow}

- **puffin9's CLI** (2023-06-03) (**NO SMP**) - [More info](https://archive.org/details/wii-u-gentoo) - [Torrent](/assets/torrents/wii-u-gentoo.torrent) - [Flashable disk image](https://archive.org/download/wii-u-gentoo/WiiUGentoo-June3rd2023-5.1GB.img.xz)

Once you have downloaded your image, proceed with the [Install](/install) guide.

## Building it yourself
> This is still Gentoo! The compilation is likely to take several days even on a powerful computer - make sure you have plenty of RAM.
{:.yellow}

1. You'll need a Gentoo setup on a machine that you'd like to use for compiling. It does **not** have to be a PowerPC, x86 or Arm will work fine. A good option if you don't want to install Gentoo is to use [a Chroot](https://wiki.gentoo.org/wiki/Chroot) from an existing Linux or WSL setup. systemd-nspawn makes this very convenient :)
2. Next, follow the [crossdev Installation instructions](https://wiki.gentoo.org/wiki/Crossdev#Installation) (you should be able to run `crossdev --help`)
3. Download the [linux-wiiu SMP patches](https://gitlab.com/linux-wiiu/smp-patches). Even if you don't plan to use SMP, it doesn't hurt to include them since you're compiling anyway. You need the glibc and gcc patch.
4. Follow the [SMP patches setup for Gentoo](https://gitlab.com/linux-wiiu/smp-patches#using-gentoo) instructions. You'll have to create the folders in `/usr/powerpc-espresso-linux-gnu/etc/portage` when they come up. You should now have `powerpc-espresso-linux-gnu-gcc` and `powerpc-espresso-linux-gnu-emerge`.
5. Optionally, edit `/usr/powerpc-espresso-linux-gnu/etc/portage/make.conf` and change `ACCEPT_KEYWORDS="${ARCH} ~${ARCH}"` to `ACCEPT_KEYWORDS="${ARCH}"`. This will ensure you only get *stable* packages instead of testing ones, which might not work. If you'd like testing packages, skip this step.
5. Back to the Gentoo wiki, follow [the instructions for building packages](https://wiki.gentoo.org/wiki/Crossdev#Build_packages_with_crossdev) substituting `powerpc-espresso-linux-gnu` for the aarch64 example they have. Set profiles then build the base system - Use the manual build, not a stage3 tarball, since that won't have the patched compiler.
6. Build `@system`!
7. `/usr/powerpc-espresso-linux-gnu` is now a usable PowerPC rootfs. You can tarball it up, copy it to an ext4 partition, NFS-root it, whatever.
8. You will have binpkgs in `/usr/powerpc-espresso-linux-gnu/var/cache/binpkgs` which you could use to set up a repository and avoid doing compilation work on your Wii U.

At some point in the future, linux-wiiu is likely to provide SMP-enabled binpkgs for Gentoo for regular users to enjoy without melting their Wii U. At that point this page will be updated with guidance on that aspect.

In addition to the linux-wiiu Matrix/Discord, YouTuber Cursed Silicon has been pushing forward Gentoo on Wii U development a fair bit, so [his Discord](https://discord.gg/TnpSG2P677) has some knowledgable people in it.
