---
layout: default
title: linux-wiiu
---
# Welcome to linux-wiiu!
linux-wiiu aims to allow access to the Wii U's unique hardware from the familiar Linux operating environment.

If you'd like to try linux-wiiu for yourself, follow the <a href="/install">Install</a> guide.

> Content is still being moved from the old wiki, so some pages are missing. Sorry about this :(
> In the meantime, the [README on GitLab](https://gitlab.com/linux-wiiu/linux-wiiu/-/blob/rewrite-4.19/README.md) has a fair bit of information.
{:.yellow}

## Quick Links
- [![](/assets/img/icons/tux.webp){:.inline title="Tux by Larry Ewing using The GIMP"} Kernel source](https://gitlab.com/linux-wiiu/linux-wiiu)
- [![](/assets/img/icons/adelie.webp){:.inline} Adélie](/adelie)
- [![](/assets/img/icons/arch.webp){:.inline} ArchPOWER](/archpower)
- [![](/assets/img/icons/ubuntu.webp){:.inline} Ubuntu](/ubuntu)
- [![](/assets/img/icons/debian.webp){:.inline} Debian](/debian)
- [![](/assets/img/icons/gentoo.webp){:.inline} Gentoo](/gentoo)
- [![](/assets/img/icons/void.webp){:.inline} Void Linux](/void)

## Pics
<picture>
<source srcset="/assets/img/pics/archpower.webp">
<img src="/assets/img/pics/archpower.jpg" alt="A monitor showing a web browser and FrogFind, with an Arch neofetch in the corner. A Wii U GamePad sits below it with some Xorg log output.">
</picture>
ArchPOWER XFCE desktop, via ccf_100 on Discord

<picture>
<source srcset="/assets/img/pics/macos9.webp">
<img src="/assets/img/pics/macos9.jpg" alt="A photo of a Classic Mac OS desktop, with an article - 'The Unholy Saga of Phoenix Hyperspace' - open in Netscape.">
</picture>
Mac OS 9 running under SheepShaver, via [quarky](https://heyquark.com)

<picture>
<source srcset="/assets/img/pics/adelie-tde.webp">
<img src="/assets/img/pics/adelie-tde.png" alt="A screenshot of a classic blue desktop, with an Adélie neofetch and Konqueror browser on the Wikipedia article for the Wii U.">
</picture>
TDE desktop on Adélie via [quarky](https://heyquark.com)

<picture>
<source srcset="/assets/img/pics/minecraft.webp">
<img src="/assets/img/pics/minecraft.png" alt="The Minecraft Java Edition multiplayer server selector. 'Running Minecraft on the WiiU since 2023!' and 'Xbox 360 Server!!'">
</picture>
Cuberite minecraft server via i_lost_my_bagel on Discord - and yes, the TPS is playable!

<picture>
<source srcset="/assets/img/pics/adelie.webp">
<img src="/assets/img/pics/adelie.png" alt="A starry desktop with terminals compiling code for PowerPC.">
</picture>
Adélie IceWM desktop via the [Adélie Linux website](https://www.adelielinux.org/screenshots/)
