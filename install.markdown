---
layout: default
title: Install - linux-wiiu
---
# Installation guide
This guide will cover the steps to install linux-wiiu on a Nintendo Wii U. It is assumed that you already have Cafe OS homebrew set up and working.

> If you don't already have homebrew, please [set up Tiramisu](https://wiiu.hacks.guide) (or Aroma) before continuing.
{:.yellow}

## Choosing a storage device
You will need an SD card for Cafe OS homebrew and for the kernel files, but you have more options when it comes to the storage medium for the distro itself.

- A **USB storage** device (like a flash drive) is easy to set up with disk images, but since the Wii U only supports USB 2.0, the system might feel slow and unresponsive. There are also hardware compatibility concerns with USB hard drives (search for [Y Cables](https://duckduckgo.com/?q=wii+u+y+cable)).
- An **SD card** partition performs a lot better and has no compatibility issues, but can be more difficult to set up since you have to manually partition the card instead of using disk images. Make sure you have a good-quality card!
- A **2.5" SATA storage** device like a hard drive or solid state drive provides the best performance the Wii U has to offer, but requires installation of a [SATA adapter board](https://github.com/ashquarky/usata) and removal of the optical drive. This obviously affects Cafe OS's function.

## linux-loader
**linux-loader** replaces the Cafe OS IOSU and is in charge of loading and booting the Linux kernel.

1. [Download linux-loader](https://gitlab.com/linux-wiiu/linux-loader#getting-linux-loader). You should have a file named `fw.img`.
2. Insert the Wii U's SD card into your computer or connect over FTP. Using the Aroma beta's FTP plugin, the SD card can be found at `/fs/vol/external01`.
3. Copy `fw.img` to the **root of the SD card**.

## Linux kernel
The **Linux kernel** is the main component of linux-wiiu, and is what contains all the drivers for the Wii U's hardware and allows normal Linux programs to run on it.

This guide will use the most complete linux-wiiu kernel, which at the time of writing is **version 4.19**, but development versions should be similar.

1. [Download the kernel](https://gitlab.com/linux-wiiu/linux-wiiu#getting-linux). You should have a file named `dtbImage.wiiu`.
2. Create a folder named `linux` on the root of the SD card.
3. Place `dtbImage.wiiu` inside this folder. The final path is thus **(SD root)** -> **linux** -> **dtbImage.wiiu**.

At this point the kernel can be booted but will panic immediately since it can't find a distro with programs to run.

## Linux distribution
The **distro** is the set of programs you'll run on your Wii U, and defines how the system looks and what it can do. Some distros have graphical desktops, while others might have a lightweight shell.

Distributions that work with linux-wiiu come in two broad flavours - **flashable disk images** and **tarballs**.

### Flashable disk images
Flashable disk images will overwrite everything on a storage device and replace it with the Linux distro. Their major benefit is that you don't have to do any extra setup to preserve your data, since they delete all the data by nature. Recommended for **USB storage**. Do NOT use for SD cards - the Cafe OS homebrew will be deleted!

> In case that wasn't clear, flashable images will **delete everything on your storage device**!
{:.yellow}

1. Download and open software that can flash disk images for you. If you don't have one, [Etcher](https://etcher.balena.io/) is a fine choice.
2. Insert the USB drive into your PC, and copy off any data you don't want to lose.
3. In your flashing software, select the disk image and then select the USB drive to flash.
4. Start the flashing process.

### Tarballs
Tarballs - usually `.tar.xz` files - contain all the files for the Linux distro, but need to be unpacked into an appropriate filesystem before they can be used. Required for **SD card** partitions.

(TODO) This part of the guide is not yet written. In short:

1. Partition
2. Format as ext4 **with `-O ^metadata_csum,^64bit`** (or you'll get unsupported features when mounting)
3. Unpack tarball
4. Possibly chroot in with qemu-binfmt to set root password. May also be possible to use `init=/bin/sh` trick.
5. Unmount, sync, eject.
6. For SD cards, set `root=/dev/mmcblk0p2` in [linux-loader boot config file](https://gitlab.com/linux-wiiu/linux-loader#advanced-setup-bootcfg).

## Booting
At this point, you're ready to go!

1. Reinsert your SD card to the Wii U and turn it on.
2. **Hold B on the Gamepad** during boot. You should see a white-on-black menu. If you see the Environment Loader menu or the Tiramisu/Aroma boot menus, reboot and try again.
3. Navigate to `fw_img_loader` and press A.
4. Linux should now boot. Connect a USB keyboard and other peripherals to use it.

Have fun! Make sure to send us a picture of your working system on Matrix or Discord :)
