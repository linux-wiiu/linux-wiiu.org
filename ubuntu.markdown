---
layout: default
title: Ubuntu - linux-wiiu
---
# Ubuntu
[Ubuntu](https://ubuntu.com/download) is an extremely popular distribution that you've probably heard of before. Unfortunately, the last version to support the PowerPC processor used on Wii U is **16.10** - from 2016.

> Ubuntu on PowerPC should be considered "retro" - fun to mess around with, but it's so out of date that you're unlikely to have a good experience e.g. using the Internet.
{:.yellow}

## Downloads
Third-party images are available:

> The linux-wiiu project has a large server seeding these images, so please use torrent if you can.
{:.yellow}

- **puffin9's 16.04.5 CLI** (2022-??-??) - [More Info](https://archive.org/details/wii-u-ubuntu-cli-16.04.5.img) - [Torrent](/assets/torrents/wii-u-ubuntu-cli-16.04.5.img.torrent) - [Flashable disk image](https://archive.org/download/wii-u-ubuntu-cli-16.04.5.img/Wii_U_ubuntu_CLI_16.04.img.xz)
- **puffin9's 16.10 CLI** (2022-09-05) - [More Info](/assets/torrents/wii-u-ubuntu-16.10-cli.torrent) - [Torrent](https://archive.org/download/wii-u-ubuntu-16.10-cli/wii-u-ubuntu-16.10-cli_archive.torrent) - [Flashable disk image](https://archive.org/download/wii-u-ubuntu-16.10-cli/Wii_U_ubuntu_16.10_CLI_2GB_September_5th_2022.img.xz)

Once you have downloaded your image, proceed with the [Install](/install) guide.
