---
layout: default
title: Void Linux - linux-wiiu
---
# Void
[Void Linux](https://voidlinux.org/) is a lightweight and speedy Linux distribution known for low RAM use. There was an unofficial PowerPC port maintained by q66 until Jan 2023, which was later maintained by the [Wii Linux](https://wii-linux.org) community until it was also discontinued in September 2024. As it's a 32-bit PowerPC distro, it should also work on Wii U.

## Downloads
> The Wii Linux project used to provide images at the following links, but as of September 2024 these have been lost due to server issues. If you have these images, please reach out and I will mirror them, and let the Wii Linux guys know.
{:.yellow}

- **Wii Linux CLI** - [Tarball](http://wii-linux.org/latest_full.tar.xz) - [Packages](https://packages.wii-linux.org/)

Once you have downloaded your image, proceed with the [Install](/install) guide.
